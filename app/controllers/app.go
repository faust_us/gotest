package controllers

import (
	"github.com/revel/revel"
	"myapp/app/models"
)

type App struct {
	GormController
}

func (c App) Index() revel.Result {
	user := &models.User{Name: "Joda"}
	c.Txn.NewRecord(user)
	c.Txn.Create(user)
	return c.RenderJson(user)
}
